$('form').on('submit', function(e){
  e.preventDefault();

  var data = $(this).serialize();
  var errors, error_wrapper, error_messages;
  var url = $(this).attr('action');
  var form = $(this);
  var form_parent = form.parent();

  $('form .errors').html('');
  $('.preloader').show();

  $.ajax({
    url: url,
    type: 'post',
    data: data,
    success: function(response) {
      $('.preloader').hide();

      if (response.status=='ok') {
        if (response.redirect!=undefined) {
          window.location = response.redirect;
        } else {
          form.find("input, textarea").val("");
        }

        if (response.destroy_form!=undefined) {
          form_parent.html(response.destroy_form.message);
          form_parent.addClass('text-center');
        };
        
      } else {
        errors = response.errors;
        Object.keys(errors).forEach(function(k){
          error_messages = '';
          for (var i = errors[k].length - 1; i >= 0; i--) {
            error_messages += '<p>' + errors[k][i] + '</p>';
          };

          if (k=="__all__") {
            error_wrapper = $('#id_all');  
          } else {
            error_wrapper = $('#id_' + k).parent().find('.errors');  
          }
          error_wrapper.html(error_messages);
        });
      }
    },
    error: function(e) {
      $('.preloader').hide();
      console.log(e);
    }
  })

})

function sendFlash(flash)
{
  $('#alerts').hide().html('<div class="alert alert-'+flash.class+'">'+flash.message+'</div>').show();
}