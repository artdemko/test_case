from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm

class RegistrationForm(UserCreationForm):

  def __init__(self, *args, **kwargs):
    super(RegistrationForm, self).__init__(*args, **kwargs)
    for field in self.fields:
      self.fields[field].widget.attrs.update({'class': 'form-control'})


class LoginForm(AuthenticationForm):

  def __init__(self, *args, **kwargs):
    super(LoginForm, self).__init__(*args, **kwargs)
    for field in self.fields:
      self.fields[field].widget.attrs.update({'class': 'form-control'})