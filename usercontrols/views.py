# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import JsonResponse

from .forms import RegistrationForm, LoginForm

from django.contrib.auth import login, authenticate

def registration(request):
  context = {}

  if request.user.is_authenticated():
    return redirect('/')

  if request.method=='POST':
    response = {}
    response['status'] = 'error'
    form = RegistrationForm(request.POST)

    if form.is_valid():
      form.save()
      username = form.cleaned_data.get('username')
      password = form.cleaned_data.get('password1')
      user = authenticate(username=username, password=password)
      login(request, user)
      response['status'] = 'ok'
      response['redirect'] = '/'
    else:
      errors = form.errors
      response['errors'] = errors
      
    return JsonResponse(response)
  else:
    form = RegistrationForm()

  context['form'] = form
  return render(request, 'usercontrols/registration.html', context)


def my_login(request):
  context = {}
  response = {}
  response['status'] = 'error'

  if request.method=='POST' and request.is_ajax():
    login_form = LoginForm(data=request.POST)
    if login_form.is_valid():
      username = login_form.cleaned_data.get('username')
      password = login_form.cleaned_data.get('password')
      user = authenticate(username=username, password=password)
      login(request, user)
      response['status'] = 'ok'
      response['redirect'] = '/'
    else:
      errors = login_form.errors
      response['errors'] = errors

  return JsonResponse(response)