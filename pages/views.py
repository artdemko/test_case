# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from usercontrols.forms import LoginForm
from feedback.forms import FeedbackForm
# Create your views here.

def home(request):
  context = {}

  login_form = LoginForm()
  feedback_form = FeedbackForm()

  context['login_form'] = login_form
  context['feedback_form'] = feedback_form

  return render(request, 'pages/home.html', context)