from django.conf.urls import url
from django.contrib import admin

import pages.views
import usercontrols.views
import feedback.views

from Src import settings
from django.shortcuts import render
from django.conf.urls.static import static

urlpatterns = [
  url(r'^admin/', admin.site.urls),

  url(r'^$', pages.views.home, name="homepage"),
  url(r'^registration/$', usercontrols.views.registration, name="registration"),
  url(r'^login/$', usercontrols.views.my_login, name="login"),
  url(r'^feedback/create$', feedback.views.create, name="feedback"),
]

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

def handler404(request):
  context = {}
  return render(request, 'pages/404.html', context)

def handler500(request):
  context = {}
  return render(request, 'pages/500.html', context)