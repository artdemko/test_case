# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Feedback

class FeedbackAdmin(admin.ModelAdmin):
  list_display = ('email', 'message', 'created', 'status')
  list_filter = ('created', 'status', 'email')

admin.site.register(Feedback, FeedbackAdmin)
# Register your models here.
