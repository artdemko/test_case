# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Feedback
from django.core.mail import send_mail

from Src.settings import DEFAULT_FROM_EMAIL
from django.contrib.auth.models import User

@receiver(post_save, sender = Feedback)
def send_to_admins(instance, created, **kwargs):
  if created:
    feedback = instance
    emails = list(User.objects.filter(is_superuser=True).exclude(email__isnull=True).values_list('email', flat=True))
    subject = 'MySite - Новый отзыв'
    body = '%s \n\n %s' %(feedback.message, feedback.email)

  try:
    feedback.status=send_mail(subject, body, DEFAULT_FROM_EMAIL, emails, fail_silently=False)
    feedback.save()
  except Exception, e:
    pass
    