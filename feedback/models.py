# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

STATUS_CHOICES = (
  (0, "Не отправлено"),
  (1, "Отправлено"),
)

# Create your models here.
class Feedback(models.Model):
  class Meta:
    verbose_name = "Сообщение"
    verbose_name_plural = "Сообщения"
    ordering = ["-created",]

  user = models.ForeignKey(User, verbose_name="Пользователь")
  email = models.EmailField(max_length=50, verbose_name="Email")
  message = models.TextField(max_length=400, verbose_name="Сообщение")
  status = models.IntegerField(choices=STATUS_CHOICES, default=0, verbose_name="Статус сообщения")

  created = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
  updated = models.DateTimeField(auto_now=True, verbose_name="Дата изменения")

  def __str__(self):
    return self.message