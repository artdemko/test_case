# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.forms import ModelForm
from .models import Feedback

# Create the form class.
class FeedbackForm(ModelForm):
  class Meta:
    model = Feedback
    fields = ['email', 'message']
    labels = {
      "email": "Ваш Email",
    }

  def __init__(self, *args, **kwargs):
    super(FeedbackForm, self).__init__(*args, **kwargs)
    for field in self.fields:
      self.fields[field].widget.attrs.update({'class': 'form-control'})

