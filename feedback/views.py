# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse

from .forms import FeedbackForm
from django.middleware.csrf import get_token

def create(request):
  response = {}
  response['status'] = 'error'

  if request.method=='POST':
    form = FeedbackForm(request.POST)
    if form.is_valid():
      feedback = form.save(commit=False)
      feedback.user = request.user
      feedback.save()
      response['status'] = 'ok'
      response['destroy_form'] = {'message': "Сообщение отправлено! <br> <a href='/'>Можете отправить еще</a>"}
    else:
      errors = form.errors
      response['errors'] = errors
  return JsonResponse(response)